#include "../include/RRT_Move.h"

VERTEX *getVerMem(conf_CoSMO *robot, int typeOfMove, VERTEX* nn){
	VERTEX *p_pom;
	p_pom = (VERTEX *) malloc(sizeof(VERTEX));
	if(p_pom == NULL){
		printf("Malo pameti\n");
		exit(1);
	}
	p_pom->robot = robot;
	p_pom->typeOfMove = typeOfMove;
	p_pom->nearestNeighbor = nn;
	return p_pom;
}

POLIGON *getPoligons(char const *argv, int finish){
	int i, j, k, l;
	POLIGON *p;
	FILE *fr3;
	fr3 = fopen(argv,"r");
	if(finish==1){
		p = (POLIGON *) malloc(1*sizeof(POLIGON));
		l = 1;
	}else{
		p = (POLIGON *) malloc((numOfPol)*sizeof(POLIGON));
		l = numOfPol;
	}
	i = 0;
	while(fscanf(fr3, "%d %d\n", &(p+i)->nvert, &j) != EOF && i<l){

		(p+i)->vertX = (double *) malloc((p+i)->nvert*sizeof(double));
		(p+i)->vertY = (double *) malloc((p+i)->nvert*sizeof(double));
		for(k=0;k<(p+i)->nvert;k++){
			double X1, Y1;
			fscanf(fr3, "%lf %lf\n", &X1, &Y1);
			(p+i)->vertX[k] = X1;
			(p+i)->vertY[k] = Y1;
		}
		i++;
	}

	fclose(fr3);
	//MyPrint(p, i);
	return p;
}

void setConf_CoSMO(conf_CoSMO *robot){
	int i;
	for(i=0;i<numOfModuls;i++){
		setPosition(robot[i], i); 
	}
}

void readUparam(){
	int i,j;
	FILE *fr;
	char *robotType[4];
	#ifdef MetaCentrum
		robotType[0] = "movement/move_right.txt";
		robotType[1] = "movement/move_left.txt";
		robotType[2] = "movement/move_front.txt";
		robotType[3] = "movement/move_back.txt";
	#else
		robotType[0] = "../input/movement/move_right.txt";
		robotType[1] = "../input/movement/move_left.txt";
		robotType[2] = "../input/movement/move_front.txt";
		robotType[3] = "../input/movement/move_back.txt";
	#endif
	q_speed = (Speed **) malloc(4*sizeof(Speed *));
	for(i = 0; i<4; i++){
		fr = fopen(robotType[i],"r");
		q_speed[i] = (Speed *) malloc(numOfModuls*sizeof(Speed));
		for(j = 0; j < numOfModuls; j++){
			float input;
			fscanf(fr, "%f", &input);
			q_speed[i][j].A = input;
			fscanf(fr, "%f", &input);
			q_speed[i][j].om = input;
			fscanf(fr, "%f", &input);
			q_speed[i][j].fi = input;
			fscanf(fr, "%f\n", &input);
			q_speed[i][j].B = input;
		}
	}
	fclose(fr);
}

SPACE getVector(double fi, double l1, double l2){
	SPACE s;
	s.coordinates[0] = l1*cos(fi) - l2*sin(fi);
 	s.coordinates[1] = l2*cos(fi) + l1*sin(fi);
 	return s;
}

void MyPrint(POLIGON *p, int max){
	int i, j;
	for (i= 0; i < max; i++){
		for(j = 0; j<(p+i)->nvert; j++){
			printf("(p+%d)[%d] = [%3.1f, %3.1f]\n",i,j,(p+i)->vertX[j], (p+i)->vertY[j]);
		}
		printf("\n");
	}
}

Posun canMove(conf_CoSMO *x){
	/* TODO*/
	return YES;
}

Posun isFinish(dReal *q1, SPACE q2){
	if(sizeEdge(q1, q2)<=5.0){
		return FINISH;
	}
	return NO;
}

double sizeEdge(dReal *q1, SPACE q2){
	int j;
	double d_q;
	d_q = 0;
	for(j=0;j<DIM;j++){
			d_q += pow((q1[j] - q2.coordinates[j]), 2);
		}
	return sqrt(d_q);
}

int pnpoly(int nvert, double *vertx, double *verty, double testx, double testy){
  int i, j, h = 0;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
     (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) ){
    	h = !h;
   	}
  }
  return h;
}

SPACE SpaceSum(SPACE a, SPACE b){
	SPACE c_this;
	int i;
	for(i=0; i<DIM; i++){
		c_this.coordinates[i] = a.coordinates[i] + b.coordinates[i];
	}
	return c_this;
}

VERTEX* expandWay_cross(VERTEX *ver, SPACE s_end){

	conf_CoSMO *c_this; //[numOfModuls];
	int i, j;
	double dist, best_dist = 2*sqrt(boundX*boundX + boundY*boundY);
	VERTEX *nn;

	c_this = (conf_CoSMO *) malloc(numOfModuls*sizeof(conf_CoSMO));

	for(i=0;i<4;i++){
		setConf_CoSMO(ver->robot);
		dist = moveRobot(i, s_end);
		//printf("dist = %f, best_dist = %f\n", dist, best_dist);
		if(dist<best_dist){
			for(j=0;j<numOfModuls;j++){
				c_this[j] = getPosition(j);
			}
			nn = getVerMem(c_this, i, ver);
			best_dist = dist;
		}
	}
	return nn;
}

double moveRobot(int typeOfMove, SPACE s_end){
	/* TODO
	 * sem by sa hodilo dať niečo v zmysle že sa robot iba pohne o nejakú tú vzdialenosť, zaznamená sa jeho
	 * konfikurácia a koľko prešiel, návratová hodnota môže byť vzdialenosť ktorú prešiel	
	 */
	double this_t;
	setUparam(q_speed[typeOfMove]);
	int configX = 1, configSign = 1;

	switch(typeOfMove){
		case 1: {
			configX = 1, configSign = -1;
			break;
		}
		case 2: {
			configX = 0, configSign = 1;
			break;
		}
		case 3: {
			configX = 0, configSign = -1;
			break;
		}
	}
	printf("Passed = %f\n\n", configSign*returnDistance(configX, 0, 0));

	dReal *cf_par = calldBodyGetPosition();
		
	return sizeEdge(cf_par, s_end);
}

void writeRobotCongif(conf_CoSMO *r){
	FILE *fw;
	fw = fopen("log.txt","w");
	int i, j;
	for(i=0;i<numOfModuls;i++){
		//printf("Som tu %d\n", i);
		for(j=0; j<2; j++){
			fprintf(fw, "\nPozicia %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf]\n", i, j,
				r[i].pos[j].coordinates[0], r[i].pos[j].coordinates[1], 
				r[i].pos[j].coordinates[2]);
			fprintf(fw, "\nMatrix %dteho modulu a %dteho L_ka = \n\n\t[%lf, %lf, %lf]\n\t[%lf, %lf, %lf] \n\t[%lf, %lf, %lf] \n\t[%lf, %lf, %lf]\n", 
				i, j, r[i].R[j][0], r[i].R[j][1], r[i].R[j][2], r[i].R[j][3], r[i].R[j][4], r[i].R[j][5], 
				r[i].R[j][6], r[i].R[j][7], r[i].R[j][8], r[i].R[j][9], r[i].R[j][10], 
				r[i].R[j][11]);
			fprintf(fw, "\nQuaternion %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf, %lf]\n", i, j,
				r[i].quat[j][0], r[i].quat[j][1], r[i].quat[j][2], r[i].quat[j][3]);
			fprintf(fw, "\nLin_vel %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf]\n", i, j,
				r[i].lin_vel[j][0], r[i].lin_vel[j][1], r[i].lin_vel[j][2]);
			fprintf(fw, "\nAng_vel %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf]\n", i, j,
				r[i].ang_vel[j][0], r[i].ang_vel[j][1], r[i].ang_vel[j][2]);
			fprintf(fw, "\n\n");
		}	
	}
	fclose(fw);
}

void writeRobotCongif2(conf_CoSMO *r){
	//FILE *fw;
	//fw = fopen("log2.txt","w");
	int i, j;
	for(i=0;i<numOfModuls;i++){
		for(j=0; j<2; j++){
			printf("\nPozicia %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf]\n", i, j,
				r[i].pos[j].coordinates[0], r[i].pos[j].coordinates[1], 
				r[i].pos[j].coordinates[2]);
			printf("\nMatrix %dteho modulu a %dteho L_ka = \n\n\t[%lf, %lf, %lf]\n\t[%lf, %lf, %lf] \n\t[%lf, %lf, %lf] \n\t[%lf, %lf, %lf]\n", 
				i, j, r[i].R[j][0], r[i].R[j][1], r[i].R[j][2], r[i].R[j][3], r[i].R[j][4], r[i].R[j][5], 
				r[i].R[j][6], r[i].R[j][7], r[i].R[j][8], r[i].R[j][9], r[i].R[j][10], 
				r[i].R[j][11]);
			printf("\nQuaternion %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf, %lf]\n", i, j,
				r[i].quat[j][0], r[i].quat[j][1], r[i].quat[j][2], r[i].quat[j][3]);
			printf("\nLin_vel %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf]\n", i, j,
				r[i].lin_vel[j][0], r[i].lin_vel[j][1], r[i].lin_vel[j][2]);
			printf("\nAng_vel %dteho modulu a %dteho L_ka = \n[%lf, %lf, %lf]\n", i, j,
				r[i].ang_vel[j][0], r[i].ang_vel[j][1], r[i].ang_vel[j][2]);
			printf("\n\n");
		}	
	}
	//fclose(fw);
}