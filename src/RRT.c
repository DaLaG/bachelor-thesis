#include "../include/RRT.h"
//#include <time.h>

//int maX;
conf_CoSMO *c;


int RRTmain(int K, int isDeleted){
	SPACE q_init;	

	numOfPol = 0;
	createMyWorld(isDeleted);

	#ifdef MetaCentrum
		//q_end = getPoligons("finish.txt",1);
		//pMain = getPoligons("poligons.txt",0);
		createMyRobot("movement/robot_conf.txt");
	#else
		//q_end = getPoligons("../input/finish.txt",1);
		//pMain = getPoligons("../input/poligons.txt",0);
		createMyRobot("../input/movement/robot_conf.txt");
	#endif	
		
	numOfModuls = getNumOfPrim();
	c = (conf_CoSMO *) malloc(numOfModuls*sizeof(conf_CoSMO));
	
	readUparam();
	int rrt;
	rrt = RRT(q_init, NULL, K);
	
	free(c);
	closeMyWorld();
	
	return rrt;
}

int RRT(SPACE q_init, POLIGON *end, int K){
	SPACE q_rand, finishimo;
	
	int i, j, q_nearest, ij, maX;
	double sE;
	Posun x = NO, isEnd = NO;

	finishimo.coordinates[0] = 130.0; //rand()%boundX;
	finishimo.coordinates[1] = 110.0; //rand()%boundY;
	finishimo.coordinates[2] = 0.5;

	for(i=0;i<numOfModuls;i++){
		c[i] = getPosition(i); 		//do c sa zapise sa pociatocna poloha
	}
	
	p_G = (VERTEX **) malloc((10*(K)+1)*sizeof(VERTEX *));

	struct MultiANN* kdTree = MyMultiANN(DIM);

	srand((unsigned) time(&t));

    p_G[0] = getVerMem(c, 10, NULL);

	q_init = c[0].pos[0];
		
    MyAddPoint(kdTree, q_init, 0);
    //#if 0
	i = 1;
	j = 1;
	while(i<=K && isEnd != FINISH){
		if(i!=K) {
			q_rand.coordinates[0] = rand()%(2*boundX) - boundX;
			q_rand.coordinates[1] = rand()%(2*boundY) - boundY;
			q_rand.coordinates[2] = 0.5; 
		} else {
			q_rand.coordinates[0] = 30; //rand()%(2*boundX) - boundX;
			q_rand.coordinates[1] = 10; //rand()%(2*boundY) - boundY;
			q_rand.coordinates[2] = 0.5; 
		}

		printf("qrand = [%f, %f, %f]\n", q_rand.coordinates[0], 
			q_rand.coordinates[1], q_rand.coordinates[2]);

		q_nearest = MyNearestNeighbor(kdTree, q_rand);	
		maX = j+10; //+20;
		do{ 
			printf("[i, j, q_nearest] = [%d, %d, %d]\n", i, j, q_nearest);

			p_G[j] = expandWay_cross(p_G[q_nearest], q_rand);
			MyAddPoint(kdTree, p_G[j]->robot[0].pos[0], j);
			
  			dReal cf_par[3] = {p_G[j]->robot[0].pos[0].coordinates[0],
  				p_G[j]->robot[0].pos[0].coordinates[1], p_G[j]->robot[0].pos[0].coordinates[2]};
			
			/*
		 	printf("q_rand = [%f, %f, %f]\n",q_rand.coordinates[0], 
		 		q_rand.coordinates[1], q_rand.coordinates[2]);
		 	printf("cf_par = [%f, %f, %f]\n",cf_par[0], 
		 		cf_par[1], cf_par[2]);
			*/
			q_nearest = j;
			j++;

			sE = sizeEdge(cf_par, q_rand);
			printf("sE = [%f]\n\n", sE);
			isEnd = isFinish(cf_par, finishimo);
			//free(cf_par);
			//isEnd = FINISH;
		}while(sE>5.0 && isEnd == NO && j<maX);
		
		/*if(isEnd == FINISH){	
			j--;
			writeWay(p_G[j]);
			writeG(p_G,j);
			printf("Cesta bola najdena\n");	

		}*/
		i++;
		printf("----------------------------------------------\n");
	}
	//if(isEnd == NO){
		j--;
		printf("Cesta NEbola najdena\n");
		/*FILE *fw2;
		#ifdef MetaCentrum
			fw2 = fopen("way.txt","w");
		#else
			fw2 = fopen("../output/way.txt","w");
		#endif
		fprintf(fw2, "Cesta nebola najdena\n");
  		fclose(fw2);*/
  		writeG(p_G,j);
	//}

	//#endif
	MydeletMultiANN(kdTree);
	//K = j;
	//free(pMain);
	//free(q_end);
	//for(i = 0; i<K; i++){
	//	free(p_G[i]);
	//}
	for(i = 0; i<4; i++){
		free(q_speed[i]);
	}
	free(q_speed);
	//free(p_G);
	return j;
}

void writeWay(VERTEX *p_G2){
	VERTEX *p_akt;
	FILE *fw2;
	#ifdef MetaCentrum
		fw2 = fopen("way.txt","w");
	#else
		fw2 = fopen("../output/way.txt","w");
	#endif
	p_akt =  (VERTEX *) malloc(sizeof(VERTEX));
	for(p_akt = p_G2; p_akt->nearestNeighbor != NULL; p_akt = p_akt->nearestNeighbor){
  		fprintf(fw2, "%d 0.0 0.0 0.0\n", p_akt->typeOfMove);
  		fprintf(fw2, "%f %f %f %f\n",
				p_akt->robot[0].pos[0].coordinates[0], 
				p_akt->robot[0].pos[0].coordinates[1],
				p_akt->nearestNeighbor->robot[0].pos[0].coordinates[0], 
				p_akt->nearestNeighbor->robot[0].pos[0].coordinates[1]);
  	}
  	fclose(fw2);
}

void writeG(VERTEX *p_G2[], int IMax){
	FILE *fw;
	#ifdef MetaCentrum
		fw = fopen("movement/G.txt","w");
	#else
		fw = fopen("../input/movement/G.txt","w");
	#endif
	int i;
	for(i=1;i<=IMax;i++){
	fprintf(fw, "%f %f %f %f\n",
				p_G2[i]->robot[0].pos[0].coordinates[0], 
				p_G2[i]->robot[0].pos[0].coordinates[1],
				p_G2[i]->nearestNeighbor->robot[0].pos[0].coordinates[0], 
				p_G2[i]->nearestNeighbor->robot[0].pos[0].coordinates[1]);
	}

	fclose(fw);
}