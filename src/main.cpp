#include "../include/main.h"

/* typeOfRobot:
 * 0 -> Snake4
 * 1 -> Snake5
 * 2 -> Snake6
 * 3 -> Cross
 * 4 -> S-robot
 * 5 -> Dog
 */
	
	
int main(int argc, char *argv[]){

#if 0	

	/*int K = 0, typeOfRobot = 0;
	
	K = atoi(argv[1]);
	typeOfRobot = atoi(argv[2]);

	readDeleteModules();
	
	double per;
	per = callRRT(K,0);
	int res = isMovementGood(K, per, typeOfRobot, 0);
	
	if(res){
		printf("\n\nPreucenie bolo uspesne");
	}else{
		printf("Delete modules\n");
		FILE *frw;
		#ifdef MetaCentrum
			frw = fopen("movement/deleteModuls.txt", "r");
		#else
			frw = fopen("../input/movement/deleteModuls.txt", "r");
		#endif
		int pocetDelModulov;
		fscanf(frw, "%d ", &pocetDelModulov);
		int *numsOfModuls;
		numsOfModuls = (int*) malloc(pocetDelModulov*sizeof(int));
		for(int i = 0; i<pocetDelModulov; i++){
			fscanf(frw, "%d ", &numsOfModuls[i]);
		}
		fclose(frw);
		deleteModuls(numsOfModuls, pocetDelModulov);
		per = callRRT(K,1);
		res = isMovementGood(K, per, typeOfRobot, 1);
		//printf("per = %f, K = %d, typeOfRobot = %d\n", per, K, typeOfRobot);
		if(res){
			printf("\n\nZmazanie modulov a nasledne preucenie bolo uspesne\n\n");
		}else{
			printf("\n\nRobot nie je schopny priblizne rovnakeho planovania\n\n");
		}
	}		*/
		readDeleteModules();
		callPSO(1, 0, 0);
		FILE *frw;
		#ifdef MetaCentrum
			frw = fopen("movement/deleteModuls.txt", "r");
		#else
			frw = fopen("../input/movement/deleteModuls.txt", "r");
		#endif
		int pocetDelModulov;
		fscanf(frw, "%d ", &pocetDelModulov);
		int *numsOfModuls;
		numsOfModuls = (int*) malloc(pocetDelModulov*sizeof(int));
		for(int i = 0; i<pocetDelModulov; i++){
			fscanf(frw, "%d ", &numsOfModuls[i]);
		}
		fclose(frw);
		deleteModuls(numsOfModuls, pocetDelModulov);
		
		//for(int i=0;i<4;i++){
			callPSO(0, 0, 1);
		//}
#else
	//for(int i=0; i<4; i++){

	/*FILE *frw;
		#ifdef MetaCentrum
			frw = fopen("movement/deleteModuls.txt", "r");
		#else
			frw = fopen("../input/movement/deleteModuls.txt", "r");
		#endif
		int pocetDelModulov;
		fscanf(frw, "%d ", &pocetDelModulov);
		int *numsOfModuls;
		numsOfModuls = (int*) malloc(pocetDelModulov*sizeof(int));
		for(int i = 0; i<pocetDelModulov; i++){
			fscanf(frw, "%d ", &numsOfModuls[i]);
		}
		fclose(frw);
		deleteModuls(numsOfModuls, pocetDelModulov);
		*/

	//readDeleteModules();
	callPSO(1,0,0);
	//}

#endif
	return 1;
}

double callRRT(int K, int isDeleted){
	int k2 = RRTmain(K, isDeleted);

	double per;
	per = getPercentil(k2);
	
	//printf("\n\nPerentil je %lf\n\n", per);
	for(int i = 0; i<k2; i++){
		free(p_G[i]);
	}
	free(p_G);
	printf("\n\nPerentil je %lf\n\n", per);
	
	return per;
} 

int isMovementGood(int K, double per, int typeOfRobot, int isDeleted){
	int maxI = 0;
	switch(typeOfRobot){
		// Snake4
		case 0: {
			if(per<5.0){
				printf("Bohužial percentil %f < %f\n\n", per, 5.0);
				maxI = 2;
			}else{
				printf("\n\nAno dal som to ano percentil %f > %f\n\n", per, 5.0);
				return 1;
			}
			break;
		}
		// Snake5
		case 1: {
			if(per<5.0){
				printf("Bohužial percentil %f < %f\n\n", per, 5.0);
				maxI = 2;
			}else{
				printf("\n\nAno dal som to ano percentil %f > %f\n\n", per, 5.0);
				return 1;
			}
			break;
		}
		// Snake6
		case 2: {
			if(per<5.0){
				printf("Bohužial percentil %f < %f\n\n", per, 5.0);
				maxI = 2;
			}else{
				printf("\n\nAno dal som to ano percentil %f > %f\n\n", per, 5.0);
				return 1;
			}
			break;
		}
		// Cross
		case 3: {
			if(per<50.0){
				printf("Bohužial percentil %f < %f\n\n", per, 50.0);
				maxI = 4;
			}else{
				printf("\n\nAno dal som to ano percentil %f > %f\n\n", per, 50.0);
				return 1;
			}
			break;
		}
		// S-robot
		case 4: {
			if(per<35.0){
				printf("Bohužial percentil %f < %f\n\n", per, 35.0);
				maxI = 4;
			}else{
				printf("\n\nAno dal som to ano percentil %f > %f\n\n", per, 35.0);
				return 1;
			}
			break;
		}
		// Dog
		case 5: {
			if(per<30.0){
				printf("Bohužial percentil %f < %f\n\n", per, 30.0);
				maxI = 4;
			}else{
				printf("\n\nAno dal som to ano percentil %f > %f\n\n", per, 30.0);
				return 1;
			}
			break;
		}
	}
	
	for(int i=0;i<maxI;i++){
		callPSO(i, 1, isDeleted);
	}
	
	double newPer = callRRT(K, isDeleted);
	
	if(per>=newPer){
		printf("\n\nNeda sa nic robit newPer %f < ako per %f \n\n", newPer, per);
		return 0;
	}else{
		printf("\n\nRekurze\n\n");
		return isMovementGood(K, newPer, typeOfRobot, isDeleted);
		//per = newPer;
	}
}

void callPSO(int i, int k, int isDeleted){
	char *robotType[4];
	int typeOfMove; 
	
	#ifdef MetaCentrum
		switch(i){
			case 0: {
				robotType[0] = "movement/move_right.txt";
				robotType[1] = "movement/robot_conf.txt";	
				robotType[2] = "movement/conv/convergence_move_right.txt";
				robotType[3] = "movement/conv/convergence_move_right_everyParticle.txt";
				typeOfMove = 0;
				break;
			}case 1: {
				robotType[0] = "movement/move_left.txt";
				robotType[1] = "movement/robot_conf.txt";	
				robotType[2] = "movement/conv/convergence_move_left.txt";
				robotType[3] = "movement/conv/convergence_move_left_everyParticle.txt";
				typeOfMove = 1;
				break;
			}
			case 2: {
				robotType[0] = "movement/move_front.txt";
				robotType[1] = "movement/robot_conf.txt";	
				robotType[2] = "movement/conv/convergence_move_front.txt";
				robotType[3] = "movement/conv/convergence_move_front_everyParticle.txt";
				typeOfMove = 2;
				break;
			}
			case 3: {
				robotType[0] = "movement/move_back.txt";
				robotType[1] = "movement/robot_conf.txt";	
				robotType[2] = "movement/conv/convergence_move_back.txt";
				robotType[3] = "movement/conv/convergence_move_back_everyParticle.txt";
				typeOfMove = 3;
				break;
			}			
		}
	#else
		switch(i){
			case 0: {
				robotType[0] = "../input/movement/move_right.txt";
				robotType[1] = "../input/movement/robot_conf.txt";	
				robotType[2] = "../input/movement/conv/convergence_move_right.txt";
				robotType[3] = "../input/movement/conv/convergence_move_right_everyParticle.txt";
				typeOfMove = 0;
				break;
			}case 1: {
				robotType[0] = "../input/movement/move_left.txt";
				robotType[1] = "../input/movement/robot_conf.txt";	
				robotType[2] = "../input/movement/conv/convergence_move_left.txt";
				robotType[3] = "../input/movement/conv/convergence_move_left_everyParticle.txt";
				typeOfMove = 1;
				break;
			}
			case 2: {
				robotType[0] = "../input/movement/move_front.txt";
				robotType[1] = "../input/movement/robot_conf.txt";	
				robotType[2] = "../input/movement/conv/convergence_move_front.txt";
				robotType[3] = "../input/movement/conv/convergence_move_front_everyParticle.txt";
				typeOfMove = 2;
				break;
			}
			case 3: {
				robotType[0] = "../input/movement/move_back.txt";
				robotType[1] = "../input/movement/robot_conf.txt";	
				robotType[2] = "../input/movement/conv/convergence_move_back.txt";
				robotType[3] = "../input/movement/conv/convergence_move_back_everyParticle.txt";
				typeOfMove = 3;
				break;
			}			
		}
	#endif
	mainPSO(0, NULL, k, robotType, typeOfMove, isDeleted);
}

double getPercentil(int IMax){
	int sum=0;
	
	int accurate = 5, dif;
	dif = 100/accurate;
	int** s;
	
	s = (int **) malloc (dif*sizeof(int *));
	for(int i=0; i<dif; i++){
		s[i] = (int *) malloc (dif*sizeof(int));
	}

	for(int i=0; i<dif; i++){
		for(int j=0; j<dif; j++){
			s[i][j] = 0;
		}
	}

	for(int i=0;i<IMax;i++){
		double cor[2];
		cor[0] = (p_G[i]->robot[0].pos[0].coordinates[0] + 50)/accurate;
		cor[1] = (p_G[i]->robot[0].pos[0].coordinates[1] + 50)/accurate;
		for(int j=0; j<dif; j++){
			if( cor[0] >= j && cor[0] <= j+1){
				for(int k=0; k<dif; k++){
					if(cor[1] >= k && cor[1] <= k+1){
						if(s[j][k] == 0) s[j][k]++;
					}	
				}
			}
		}
	}

	for(int i=0; i<dif; i++){
		for(int j=0; j<dif; j++){
			sum += s[i][j];
		}
	}

	for(int i=0; i<dif; i++){
		free(s[i]);
	}
	free(s);

	return 100.0*(sum/pow(dif,2));
}

void readp_G(){
	int IMax = 236;
	double a;
	FILE *fr;
	fr = fopen("G.txt","r");
	p_G = (VERTEX **) malloc(IMax*sizeof(VERTEX *));
	for(int i=0;i<IMax;i++){
		p_G[i] = (VERTEX *) malloc(sizeof(VERTEX));
		p_G[i]->robot = (conf_CoSMO *) malloc(9*sizeof(conf_CoSMO));
		fscanf(fr, "%lf", &a);
		p_G[i]->robot[0].pos[0].coordinates[0] = a;
		fscanf(fr, "%lf", &a);
		p_G[i]->robot[0].pos[0].coordinates[1] = a;
		fscanf(fr, "%lf", &a);
		fscanf(fr, "%lf\n", &a);
	}
	printf(" %lf %lf\n", p_G[IMax-1]->robot[0].pos[0].coordinates[0], p_G[IMax-1]->robot[0].pos[0].coordinates[1]);
	fclose(fr);
}

/*void deleteModuls(int *numsOfModuls, int maxN){
	int nop, *intput, *intput2; //number of primitives
	FILE *frw;
	
	#ifdef MetaCentrum
		frw = fopen("movement/robot_conf.txt", "r");
	#else
		frw = fopen("../input/movement/robot_conf.txt", "r");
	#endif

	fscanf(frw, "%d\n", &nop);
	dReal **s;
	s = (dReal **) malloc(nop*sizeof(dReal*));
	intput = (int*) malloc(nop*sizeof(int));
	intput2 = (int*) malloc(nop*sizeof(int));
	
	for(int i=0; i<nop; i++){
		s[i] = (dReal *) malloc(4*sizeof(dReal));
		
		fscanf(frw, "%f", &s[i][0]);
		fscanf(frw, "%f", &s[i][1]);
		fscanf(frw, "%f\n", &s[i][2]);
		fscanf(frw, "%f\n", &s[i][3]);
		
		fscanf(frw, "%d\n", &intput[i]);
		intput2[i] = intput[i];
	}
	fclose(frw);

	#ifdef MetaCentrum
		remove("movement/robot_conf.txt");
		frw = fopen("movement/robot_conf.txt", "w");
	#else
		remove("../input/movement/robot_conf.txt");
		frw = fopen("../input/movement/robot_conf.txt", "w");
	#endif

	int xxx=1;
	int minuss = 0;
	fprintf(frw, "%d\n", nop-maxN);

	for(int i=0; i<nop; i++){
		for (int j = 0; j < maxN; j++){
			if(intput[i]>numsOfModuls[j]){
				if(intput2[numsOfModuls[j]]==0 && intput2[i] == (numsOfModuls[j]+1)){
					intput[i] = 0;
					i = -1;
				}else{
					intput[i]--;
				}
			}
		}
	}

	for(int i=0; i<nop; i++){
		for (int j = 0; j < maxN; j++){
			if(i!=numsOfModuls[j]){
				if(intput[i]==i+1){
					intput[i] = 0;
				}
			}
		}
	}

	for(int i=0; i<nop; i++){
		for (int j = 0; j < maxN; j++){
			if(i==numsOfModuls[j]){
				xxx = 0;
				break;
			}
		}
		if(xxx){
			fprintf(frw, "%f %f %f\n", s[i][0], s[i][1], s[i][2]);
			fprintf(frw, "%f\n",s[i][3]);
			fprintf(frw, "%d\n", intput[i]);
		}else{
			xxx=1;
		}
	}
	fclose(frw);
}*/

// use for dog type robot
void deleteModuls(int *numsOfModuls, int maxN){
	int nop, *intput; //number of primitives
	FILE *frw;
	
	#ifdef MetaCentrum
		frw = fopen("movement/robot_conf.txt", "r");
	#else
		frw = fopen("../input/movement/robot_conf.txt", "r");
	#endif

	fscanf(frw, "%d\n", &nop);
	dReal **s;
	s = (dReal **) malloc(nop*sizeof(dReal*));
	intput = (int*) malloc(nop*sizeof(int));
	
	for(int i=0; i<nop; i++){
		s[i] = (dReal *) malloc(4*sizeof(dReal));
		
		fscanf(frw, "%f", &s[i][0]);
		fscanf(frw, "%f", &s[i][1]);
		fscanf(frw, "%f\n", &s[i][2]);
		fscanf(frw, "%f\n", &s[i][3]);
		
		fscanf(frw, "%d\n", &intput[i]);
	}
	fclose(frw);

	#ifdef MetaCentrum
		remove("movement/robot_conf.txt");
		frw = fopen("movement/robot_conf.txt", "w");
	#else
		remove("../input/movement/robot_conf.txt");
		frw = fopen("../input/movement/robot_conf.txt", "w");
	#endif

	int xxx=1;
	int minuss = 0;
	fprintf(frw, "%d\n", nop-maxN);

	for(int i=0; i<nop; i++){
		for (int j = 0; j < maxN; j++){
			if(intput[i]>numsOfModuls[j]){
				//if(intput[numsOfModuls[j]]==0 && intput[i] == (numsOfModuls[j]+1)){
				//	intput[i] = 0;
				//}else{
					intput[i]--;
					printf("numsOfModuls[j] = %d Znizujem modul %d na hodnotu %d\n",numsOfModuls[j], i, intput[i]);
				//}
			}
		}
	}

	for(int i=0; i<nop; i++){
		for (int j = 0; j < maxN; j++){
			if(i==numsOfModuls[j]){
				xxx = 0;
				break;
			}
		}
		if(xxx){
			fprintf(frw, "%f %f %f\n", s[i][0], s[i][1], s[i][2]);
			fprintf(frw, "%f\n",s[i][3]);
			fprintf(frw, "%d\n", intput[i]);
		}else{
			xxx=1;
		}
	}
	fclose(frw);
}