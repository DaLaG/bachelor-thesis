#include "../include/PSO.h"


//#ifdef __cplusplus
//    extern "C" {
//#endif

	int mainPSO(int argc, char *argv[],int call_PSO, char *robotType[4], int config, int isDeleted){

		createMyWorld(isDeleted);
		createMyRobot(robotType[1]);

		dimPSO = getNumOfPrim();
		swarm_P = 20;

		if(call_PSO){
			PSO_fun(robotType[2], robotType[3], 200, robotType[0], config);
		}else{
			for(int i = 0; i<1; i++){
				printf("q_R je %f \n\n\n", readUparamPSO(robotType[0], config));
				setC_OffsetConfg();
			}
			//const dReal *pos1 = calldBodyGetPosition();
			//printf("pos1 = [%f, %f, %f]\n", pos1[0], pos1[1], pos1[2]);
			callSim (argc,argv);
			
		}

		closeMyWorld();
		return 0;
	}

	int PSO_fun(char *convergPath, char *convergPath2, int I, char *robotType, int config){
		short int i, j, k, r = swarm_P;
		Speed x[swarm_P][dimPSO], p[swarm_P][dimPSO], v[swarm_P][dimPSO];
		Speed q[dimPSO];
		dReal p_R[swarm_P], x_R[swarm_P], convergR[I + swarm_P], **conP_i;
		dReal r_q, r_p, q_R, q_RPoc;
		
		conP_i = (dReal **) malloc((I+1)*sizeof(dReal*));
		
		int configX = 1, configSign = 1;

		switch(config){
			case 1: {
				configX = 1, configSign = -1;
				break;
			}
			case 2: {
				configX = 0, configSign = 1;
				break;
			}
			case 3: {
				configX = 0, configSign = -1;
				break;
			}
		}
		
		printf("		%s\n\n", convergPath);

		time_t thisT;
		srandom(time(&thisT));
		#if 1
			char *move_typeTxtString[4];
			#ifdef MetaCentrum
				move_typeTxtString[0] = "movement/move_right.txt";
				move_typeTxtString[1] = "movement/move_left.txt";
				move_typeTxtString[2] = "movement/move_front.txt";
				move_typeTxtString[3] = "movement/move_back.txt";
			#else
				move_typeTxtString[0] = "../input/movement/move_right.txt";
				move_typeTxtString[1] = "../input/movement/move_left.txt";
				move_typeTxtString[2] = "../input/movement/move_front.txt";
				move_typeTxtString[3] = "../input/movement/move_back.txt";
			#endif
			q_R = -1000000;
			q_RPoc = q_R;
			for(int move_typeTxt=0; move_typeTxt<4; move_typeTxt++){
				dReal q_R_temp;
				Speed q_temp[dimPSO];
				FILE *fr;
				fr = fopen(move_typeTxtString[move_typeTxt],"r");
				
				for(int j = 0; j < dimPSO; j++){
						
					fscanf(fr, "%f", &q_temp[j].A);
					fscanf(fr, "%f", &q_temp[j].om);
					fscanf(fr, "%f", &q_temp[j].fi);
					fscanf(fr, "%f\n", &q_temp[j].B);
				}
				setUparam(q_temp);
				fclose(fr);
				q_R_temp = configSign*returnDistance(configX, 0, 0); 
				if(q_R_temp>q_R){
					for(int j = 0; j < dimPSO; j++){
						q[j] = q_temp[j];
					}
					q_R = q_R_temp; 
					if(move_typeTxt == config){
						q_RPoc = q_R;
					}
				}
				setC_OffsetConfg();
			}
		#else
			q_R = 0;
			q_RPoc = q_R;
		#endif
		printf("q_R je %f \n", q_R);
		for(i = 0; i < swarm_P; i++){
			convergR[i] = q_R;
			for(j = 0; j < dimPSO; j++){
				x[i][j].A = ((M_PI_2/4) * (random() % RAND_MAX) / RAND_MAX);
				p[i][j].A = x[i][j].A;
				v[i][j].A = 0;
				
				x[i][j].om = 2*M_PI*(0.3 + (0.7-0.3) * (random() % RAND_MAX) / RAND_MAX);
				p[i][j].om = x[i][j].om;
				v[i][j].om = 0;
				
				x[i][j].fi = (2*M_PI) * (random() % RAND_MAX) / RAND_MAX;
				p[i][j].fi = x[i][j].fi;
				v[i][j].fi = 0;
				
				x[i][j].B = ((M_PI_2/2) * (random() % RAND_MAX) / RAND_MAX) - (M_PI_2/4);
				p[i][j].B = x[i][j].B;
				v[i][j].B = 0;
			}
			setUparam(x[i]);
			p_R[i] = configSign*returnDistance(configX, 0, 0);
			setC_OffsetConfg();
			if(p_R[i]>q_R){
				for(j = 0; j < dimPSO; j++){
					q[j] = p[i][j];
				}
				printf("\nq_R bolo %f a teraz je %f \n\n", q_R, p_R[i]);
				q_R = p_R[i];
			}
		}

			
		for(k = 0; k<=I; k++){
			conP_i[k] = (dReal *) malloc(swarm_P*sizeof(dReal));
			convergR[swarm_P + k] = q_R;
			//printf("		k = %d    %s\n", k, convergPath);
			
			for(i=0; i<swarm_P; i++){
				conP_i[k][i] = p_R[i];

				for(j=0; j<dimPSO; j++){
					
					r_p = 1.0*(random() % RAND_MAX) / RAND_MAX;
					r_q = 1.0*(random() % RAND_MAX) / RAND_MAX;
					
					v[i][j].A = OMEGA*v[i][j].A + FI_P*r_p*(p[i][j].A - x[i][j].A) + FI_Q*r_q*(q[j].A - x[i][j].A);
					v[i][j].om = OMEGA*v[i][j].om + FI_P*r_p*(p[i][j].om - x[i][j].om) + FI_Q*r_q*(q[j].om - x[i][j].om);
					v[i][j].fi = OMEGA*v[i][j].fi + FI_P*r_p*(p[i][j].fi - x[i][j].fi) + FI_Q*r_q*(q[j].fi - x[i][j].fi);
					v[i][j].B = OMEGA*v[i][j].B + FI_P*r_p*(p[i][j].B - x[i][j].B) + FI_Q*r_q*(q[j].B - x[i][j].B);
					
					x[i][j].A += v[i][j].A;
					if(x[i][j].A < 0 || x[i][j].A > (M_PI_2/4)){
						x[i][j].A -= v[i][j].A; 
					}

					x[i][j].om += v[i][j].om;
					if(x[i][j].om < 2*M_PI*0.3 || x[i][j].om > 2*M_PI*0.7){
						x[i][j].om -= v[i][j].fi; 
					}

					x[i][j].fi += v[i][j].fi;
					if(x[i][j].fi < -2*M_PI || x[i][j].fi > 2*M_PI){
						x[i][j].fi -= v[i][j].fi; 
					}

					x[i][j].B += v[i][j].B;
					if(x[i][j].B < -(M_PI_2/4) || x[i][j].B>(M_PI_2/4)){
						x[i][j].B -= v[i][j].B;
					}
				}
				x_R[i] = configSign*returnDistance(configX, 0, 0);
				setC_OffsetConfg();
				if(x_R[i]>p_R[i]){
					if(x_R[i]>q_R){
						for(j = 0; j < dimPSO; j++){
							p[i][j] = x[i][j];
							q[j] = p[i][j];
						}
						p_R[i] = x_R[i];

						printf("q_R bolo %f a teraz je %f \n", q_R, p_R[i]);
						q_R = p_R[i];
					}else{
						for(j = 0; j < dimPSO; j++){
							p[i][j] = x[i][j];
						}
						p_R[i] = x_R[i];
					}
				}
			}
		}
		writeConvergence(convergPath, convergPath2, convergR, I + swarm_P, conP_i, swarm_P);
		setC_OffsetConfg();

		if(q_RPoc<q_R){
			printResault(q);
			writeUparamToFile(robotType, q);
		}
		for(i=0; i<=I; i++){
			free(conP_i[i]);
		}
		free(conP_i);
		printf("\n\n");
	}

	dReal readUparamPSO(char *robotType, int config){
		int configX = 1, configSign = 1;

		switch(config){
			case 1: {
				configX = 1, configSign = -1;
				break;
			}
			case 2: {
				configX = 0, configSign = 1;
				break;
			}
			case 3: {
				configX = 0, configSign = -1;
				break;
			}
		}
		
		FILE *fr;
		fr = fopen(robotType,"r");
		Speed q[dimPSO];

		for(int j = 0; j < dimPSO; j++){
				
			fscanf(fr, "%f", &q[j].A);
			fscanf(fr, "%f", &q[j].om);
			fscanf(fr, "%f", &q[j].fi);
			fscanf(fr, "%f\n", &q[j].B);
		}
		setUparam(q);
		//printResault(q);
		fclose(fr);
		return configSign*returnDistance(configX, 0, 0);
		//return 1;
	}

	void writeUparamToFile(char *robotType, Speed *q){
		FILE *fr;
		fr = fopen(robotType,"w");
		
		for(int j = 0; j < dimPSO; j++){
				
			fprintf(fr, "%.15f ", q[j].A);
			fprintf(fr, "%.15f ", q[j].om);
			fprintf(fr, "%.15f ", q[j].fi);
			fprintf(fr, "%.15f\r\n", q[j].B);
		}
		fclose(fr);
	}

	void writeConvergence(char *robotType1,char *robotType2, dReal *convergR, int I, dReal **conP_i, int swarm_P){
		FILE *fr, *fr2;
		fr = fopen(robotType1,"w");
		
		fr2 = fopen(robotType2,"w");
		
		for(int j = 0; j < I; j++){
			fprintf(fr, "%lf\r\n", convergR[j]);
		}
		
		for (int i = 0; i < swarm_P; i++){
			if(i==swarm_P-1){
				fprintf(fr2, "Partic%2d\r\n", i);
			}else{
				fprintf(fr2, "Partic%2d ", i);
			}
		}

		for(int j = 0; j < I - swarm_P; j++){
			for (int i = 0; i < swarm_P; i++){
				if(i==swarm_P-1){
					fprintf(fr2, "%lf\r\n", conP_i[j][i]);
				}else{
					fprintf(fr2, "%lf ", conP_i[j][i]);
				}
			}
		}

		fclose(fr);
		fclose(fr2);
	}

	void printResault(Speed *q){
		for(int k=0; k<dimPSO; k++){
			printf("Speed parametre = [%f, %f, %f, %f]\n", q[k].A, q[k].om, q[k].fi, q[k].B);
		}
	}
//#ifdef __cplusplus
//    }
//#endif