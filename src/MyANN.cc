//#include "../include/MPNN/include/DNN/Ann.h"
#include "../include/MyANN.h"

#ifdef __cplusplus
    extern "C" {
#endif
        ANNpoint MyAnnAllocPt(int dim, ANNcoord c){        // allocate 1 point
            c = 0;
            ANNpoint p = new ANNcoord[dim];
            for (int i = 0; i < dim; i++) p[i] = c;
            return p;
        }
           
        ANNpointArray MyAnnAllocPts(int n, int dim){       // allocate n pts in dim
            ANNpointArray pa = new ANNpoint[n];         // allocate points
            ANNpoint      p  = new ANNcoord[n*dim];     // allocate space for coords
            for (int i = 0; i < n; i++) {
                pa[i] = &(p[i*dim]);
            }
            return pa;
        }

        void MyAnnDeallocPt(ANNpoint p){                 // deallocate 1 point
          delete [] p;
          p = NULL;
        }
           
        void MyAnnDeallocPts(ANNpointArray pa){           // deallocate points
          delete [] pa[0];                              // dealloc coordinate storage
          delete [] pa;                         // dealloc points

          pa = NULL;
        }
#ifdef __cplusplus
    }
#endif