/*
* Daniel Lamper
*/

#include "../include/CoSMO.h"
#ifdef __cplusplus
    extern "C" {
#endif
	static dWorldID world;			
	static dJointGroupID contactgroup;			

	static dSpaceID space;			
	static dGeomID ground;			

	static CoSMO_r *robot;			
	static dJointID *primJoint;			
	static dJointGroupID robotJointGroup;			


	static dReal steer=0;			
	double t_this = 0.0;			
	static Speed *u;
	static dsFunctions fn;			
	static int numOfPrimitivs;			
	static Offset robot_conf;
	static int *typeOfMovement;
	conf_CoSMO *c_Offset;
	static int counter = 0, isDeleted;
	using namespace std;
	int pocetFModulov;
	int *numsOfFModuls;
	int isFree;

	//struktura txt je ze prve cislo je pocet fix/free modulov potom konkretne moduli a posledn cislo urcuje ci je to free (1) alebo fix (0) typ poruchy
	void readDeleteModules(){
		FILE *frw;
		#ifdef MetaCentrum
			frw = fopen("movement/FreeFixModule.txt", "r"); 
		#else
			frw = fopen("../input/movement/FreeFixModule.txt", "r");
		#endif
		
		fscanf(frw, "%d ", &pocetFModulov);
		
		numsOfFModuls = (int*) malloc(pocetFModulov*sizeof(int));
		for(int i = 0; i<pocetFModulov; i++){
			fscanf(frw, "%d ", &numsOfFModuls[i]);
		}
		fscanf(frw, "%d ", &isFree);
		fclose(frw);
	}

	void createMyWorld(int this_isDeleted){

		dInitODE2(0);
		world = dWorldCreate();
		space = dHashSpaceCreate(0);
		contactgroup = dJointGroupCreate(0);
		
		dWorldSetGravity(world, 0, 0, -GRAVITY);

		ground = dCreatePlane(space, 0, 0, 1, 0);
		isDeleted = this_isDeleted;
	}

	int getNumOfPrim(){
		return numOfPrimitivs;
	}

	void createMyRobot(char *robotType){
		int i, j, k;
		
		dReal len[2] = {LENGTH_1, LENGTH_2}, mass[2] = {MASS_1, MASS_2}, 
			wid[2] = {WIDTH_1, WIDTH_2}, hei[2] = {HEIGHT_1, HEIGHT_2};

		const int NUM_BOXES = 2;

		dGeomID tempOdeBoxGeomList[NUM_BOXES];

		dReal boxOffset[2][3] = {0, 0.4, 0.5, 0.4, 0.15, 0.5};

		readStartPosition(&robot_conf, robotType);

		dMass compositeMass, componentMass;

		for(k=0; k<numOfPrimitivs; k++){

			robot[k].space = dSimpleSpaceCreate(space);
			dSpaceSetCleanup(robot[k].space,0);
			
			for(i = 0; i<2; i++){
				dMassSetZero(&compositeMass);
				robot[k].Ls[i].body = dBodyCreate(world);
		
				for(j=0; j<NUM_BOXES; j++){
					dMassSetZero(&componentMass);
					
					robot[k].Ls[i].geom[j] = dCreateGeomTransform(robot[k].space);
					dGeomTransformSetCleanup(robot[k].Ls[i].geom[j],1);
		
					tempOdeBoxGeomList[j] = dCreateBox(0, len[j], wid[j], hei[j]);
					//dMassSetBox(&componentMass, 1.5625,  len[j], wid[j], hei[j]);
					dMassSetBoxTotal(&componentMass, mass[j],  len[j], wid[j], hei[j]);
		
					dGeomTransformSetGeom(robot[k].Ls[i].geom[j], tempOdeBoxGeomList[j]);
		
					dGeomSetPosition(tempOdeBoxGeomList[j],
								   boxOffset[j][0], 
			                       boxOffset[j][1], 
			                       boxOffset[j][2]);
		
					dMassTranslate(&componentMass,
								   boxOffset[j][0], 
			                       boxOffset[j][1], 
			                       boxOffset[j][2]);
		
					dMatrix3 rotation;
					dRFromAxisAndAngle(rotation,0.0,0.0,0.0,0.0);
		
					dGeomSetRotation(tempOdeBoxGeomList[j], rotation);
					dMassRotate(&componentMass, rotation);
		
					dMassAdd(&compositeMass, &componentMass);
				}
		
				for(j=0; j<NUM_BOXES; j++){
				    dGeomSetPosition (tempOdeBoxGeomList[j],
				                       boxOffset[j][0]-compositeMass.c[0],
				                       boxOffset[j][1]-compositeMass.c[1],
				                       boxOffset[j][2]-compositeMass.c[2]);
				}
				dMassTranslate (&compositeMass,
				                -compositeMass.c[0],
				                -compositeMass.c[1],
				                -compositeMass.c[2]);
				
				for(j = 0; j<NUM_BOXES; j++){
					if(robot[k].Ls[i].geom[j]){
						dGeomSetBody(robot[k].Ls[i].geom[j], robot[k].Ls[i].body);
					}
				}
				dBodySetMass(robot[k].Ls[i].body, &compositeMass);
			}

			setOffsetPosRot2(k);

			callHingeJoint(&robot[k].joint, &robot[k].Ls[0].body, &robot[k].Ls[1].body, 
				robot_conf.boxOffsetAngel[k], robot_conf.boxOffset[k]);
			
			dJointSetHingeParam(robot[k].joint, dParamLoStop,-1.04);
			dJointSetHingeParam(robot[k].joint, dParamHiStop, 1.04);
			dJointSetHingeParam(robot[k].joint, dParamFMax, 40);
			dJointSetHingeParam(robot[k].joint, dParamFudgeFactor, 0.1);	
			dJointSetHingeParam(robot[k].joint, dParamBounce, 0);
		}
		jointPrimitivsRobot();
		if(!isDeleted && !isFree){
			for(int i = 0; i<pocetFModulov; i++){
				setZeroHingeParam(numsOfFModuls[i]);
			}
		}
		for(double this_t=0; this_t<1; this_t+=0.05){
			dSpaceCollide(space,0,&nearCallback);
			dWorldStep(world,0.05);
			dJointGroupEmpty(contactgroup);
		}

		getC_OffsetConfg();
	}

	void readStartPosition(Offset* thisOs, char *robotType){
		int intput;
		float input;
		FILE *fr;
		fr = fopen(robotType,"r");
		fscanf(fr, "%d\n", &numOfPrimitivs);

		robot = (CoSMO_r *) malloc(numOfPrimitivs*sizeof(CoSMO_r));
		primJoint = (dJointID *) malloc((numOfPrimitivs-1)*sizeof(dJointID));
		c_Offset = (conf_CoSMO *) malloc(numOfPrimitivs*sizeof(conf_CoSMO));
		
		u = (Speed *) malloc(numOfPrimitivs*sizeof(Speed));

		thisOs->boxOffset = (dReal **) malloc(numOfPrimitivs*sizeof(dReal*));
		thisOs->boxOffsetAngel = (dReal *) malloc(numOfPrimitivs*sizeof(dReal));
		thisOs->jointWith = (int *) malloc(numOfPrimitivs*sizeof(int));

		for(int i=0; i<numOfPrimitivs; i++){
			thisOs->boxOffset[i] = (dReal *) malloc(3*sizeof(dReal));
			
			fscanf(fr, "%f", &input);
			thisOs->boxOffset[i][0] = input;
			fscanf(fr, "%f", &input);
			thisOs->boxOffset[i][1] = input;
			fscanf(fr, "%f\n", &input);
			thisOs->boxOffset[i][2] = input;
			fscanf(fr, "%f\n", &input);
			thisOs->boxOffsetAngel[i] = input;
			
			fscanf(fr, "%d\n", &intput);
			thisOs->jointWith[i] = intput;

			//printf("[%f, %f, %f, %f, %d]\n", thisOs->boxOffset[i][0], thisOs->boxOffset[i][1],
			// thisOs->boxOffset[i][2], thisOs->boxOffsetAngel[i], thisOs->jointWith[i]);
			//setU(&u[i], i); 
		}

		fclose(fr);
	}

	void jointPrimitivsRobot(){
		robotJointGroup = dJointGroupCreate(numOfPrimitivs-1);
		int i = 0;
		for(int k=0; k<numOfPrimitivs; k++){
			primJoint[i] = dJointCreateFixed(world, 0);
			int jointWith = robot_conf.jointWith[k]-1;
			if(jointWith<0){
				continue;
			}
			if(robot_conf.boxOffsetAngel[k] == (float) 0.0){
				callFixJoint(&primJoint[i], &robot[k].Ls[0].body, &robot[jointWith].Ls[1].body);
			} else {
				if(robot_conf.boxOffsetAngel[k] == (float) 1.570796){ //M_PI_2){
					if(robot_conf.boxOffsetAngel[k] == robot_conf.boxOffsetAngel[jointWith]){
						callFixJoint(&primJoint[i], &robot[k].Ls[0].body, &robot[jointWith].Ls[1].body);
					}else{
						if(robot_conf.boxOffset[k][0]<robot_conf.boxOffset[jointWith][0]){
							callFixJoint(&primJoint[i], &robot[k].Ls[1].body, &robot[jointWith].Ls[1].body);
						}else{
							callFixJoint(&primJoint[i], &robot[k].Ls[0].body, &robot[jointWith].Ls[0].body);
						}
					}
				} else {
					if(robot_conf.boxOffsetAngel[k] == (float)  3.141592){ //M_PI){
						if(robot_conf.boxOffsetAngel[k] == robot_conf.boxOffsetAngel[jointWith]){
							callFixJoint(&primJoint[i], &robot[k].Ls[0].body, &robot[jointWith].Ls[1].body);
						}else{
							callFixJoint(&primJoint[i], &robot[k].Ls[0].body, &robot[jointWith].Ls[0].body);
						}
					} else {
						//printf("robot_conf.boxOffsetAngel[k] = %f\nM_PI_2 = %f\n", robot_conf.boxOffsetAngel[k], -1.570796);
						if(robot_conf.boxOffsetAngel[k] == (float) -1.570796){ //M_PI_2){
						//	printf("Som tu\n");
							if(robot_conf.boxOffsetAngel[k] == robot_conf.boxOffsetAngel[jointWith]){
								callFixJoint(&primJoint[i], &robot[k].Ls[0].body, &robot[jointWith].Ls[1].body);
							}else{
								if(robot_conf.boxOffset[k][0]<robot_conf.boxOffset[jointWith][0]){
									callFixJoint(&primJoint[i], &robot[k].Ls[0].body, &robot[jointWith].Ls[1].body);
								}else{
									callFixJoint(&primJoint[i], &robot[k].Ls[1].body, &robot[jointWith].Ls[0].body);
								}
							}
						}
					}
				}
			}	
			i++;
		}
	}

	void callHingeJoint(dJointID* j, dBodyID* b1, dBodyID* b2, dReal angel, dReal X[3]){
			*j = dJointCreateHinge(world, 0);
			dJointAttach(*j, *b1, *b2);
			dJointSetHingeAnchor(*j, X[0], X[1], X[2] + z0);
			dJointSetHingeAxis(*j, 1.0*cos(angel), 1.0*sin(angel), 0.0);
	}

	void callFixJoint(dJointID* j, dBodyID* b1, dBodyID* b2){
		dJointAttach(*j, *b1, *b2);
		dJointSetFixed(*j);
	}

	void setOffsetPosRot2(int k){
		dMatrix3 R_f, R_b;
		dRFromAxisAndAngle (R_f, 0.0, 0.0, 1.0, robot_conf.boxOffsetAngel[k]);
		dRFromAxisAndAngle (R_b, 0.0, 0.0, 1.0, M_PI + robot_conf.boxOffsetAngel[k]);
		
		dBodySetRotation(robot[k].Ls[0].body, R_f);
		dBodySetRotation(robot[k].Ls[1].body, R_b);
		
		dBodySetPosition(robot[k].Ls[0].body, 
			robot_conf.boxOffset[k][0] + DELAY1*cos(robot_conf.boxOffsetAngel[k]) - DELAY2*sin(robot_conf.boxOffsetAngel[k]),
			robot_conf.boxOffset[k][1] + DELAY2*cos(robot_conf.boxOffsetAngel[k]) + DELAY1*sin(robot_conf.boxOffsetAngel[k]),
			robot_conf.boxOffset[k][2] + z0);

		dBodySetPosition(robot[k].Ls[1].body, 
			robot_conf.boxOffset[k][0] + DELAY1*cos(robot_conf.boxOffsetAngel[k]+M_PI) - DELAY2*sin(robot_conf.boxOffsetAngel[k]+M_PI),
			robot_conf.boxOffset[k][1] + DELAY2*cos(robot_conf.boxOffsetAngel[k]+M_PI) + DELAY1*sin(robot_conf.boxOffsetAngel[k]+M_PI),
			robot_conf.boxOffset[k][2] + z0);
	}

	// fix in rotation the last primitiv
	void setZeroHingeParam(int i){
		dJointSetHingeParam (robot[i].joint,dParamLoStop,0);
		dJointSetHingeParam (robot[i].joint,dParamHiStop,0);
	}

	// get position means get properties like position, rotation, quaternion, linVel, angVel
	conf_CoSMO getPosition(int i){
		conf_CoSMO conf_C;
		const dReal *cf_par;
		for (int j = 0; j < 2; j++){
			cf_par = dBodyGetPosition(robot[i].Ls[j].body);
			for(int k = 0; k<DIM; k++){
				conf_C.pos[j].coordinates[k] = cf_par[k];
			}
			
			cf_par = dBodyGetLinearVel(robot[i].Ls[j].body);
			for(int k = 0; k<3; k++){
				conf_C.lin_vel[j][k] = cf_par[k];
			}

			cf_par = dBodyGetAngularVel(robot[i].Ls[j].body);
			for(int k = 0; k<3; k++){
				conf_C.ang_vel[j][k] = cf_par[k];
			}

			cf_par = dBodyGetQuaternion(robot[i].Ls[j].body);
			for(int k = 0; k<4; k++){
				conf_C.quat[j][k] = cf_par[k];
			}

			cf_par = dBodyGetRotation(robot[i].Ls[j].body);
			for(int k = 0; k<12; k++){
				conf_C.R[j][k] = cf_par[k];
			}
		}
		return conf_C;
	}

	void getC_OffsetConfg(){
		for(int i = 0; i<numOfPrimitivs; i++){
			c_Offset[i] = getPosition(i);
		}
	}

	// set position means set properties like position, rotation, quaternion, linVel, angVel
	void setPosition(conf_CoSMO conf_C, int i){
		dJointSetHingeParam(robot[i].joint, dParamVel, 0.0);
		for (int j = 0; j < 2; j++){
			dBodySetPosition (robot[i].Ls[j].body, conf_C.pos[j].coordinates[0], 
				conf_C.pos[j].coordinates[1], conf_C.pos[j].coordinates[2]);
			dBodySetRotation (robot[i].Ls[j].body, conf_C.R[j]);
			dBodySetQuaternion (robot[i].Ls[j].body, conf_C.quat[j]);
			dBodySetLinearVel (robot[i].Ls[j].body, conf_C.lin_vel[j][0], 
				conf_C.lin_vel[j][1], conf_C.lin_vel[j][2]);
			dBodySetAngularVel (robot[i].Ls[j].body,conf_C.ang_vel[j][0],
			conf_C.ang_vel[j][1], conf_C.ang_vel[j][2]);
		}
	}

	void setC_OffsetConfg(){
		for(int i = 0; i<numOfPrimitivs; i++){
			setPosition(c_Offset[i], i);
		}
	}

	void setUparam(Speed *thisU){
		//printf("\n\n");
		for (int k = 0; k < numOfPrimitivs; ++k){
			u[k].A = thisU[k].A;
			u[k].om = thisU[k].om;
			u[k].fi = thisU[k].fi;
			u[k].B = thisU[k].B;
		}
	}

	dReal returnDistance(int sur, const dReal *pos1,const dReal *pos2){
		if (!pos1) pos1 = dBodyGetPosition(robot[0].Ls[0].body);
		//printf("pos1 = [%f, %f, %f]\n", pos1[0], pos1[1], pos1[2]);
		printf("%f %f %f\n", pos1[0], pos1[1], pos1[2]);
		dReal pos = pos1[sur];
		for(double this_t=0; this_t<15; this_t+=0.05){
			//printf("%lf\n", this_t);
			dSimLoop(this_t);
			pos1 = dBodyGetPosition(robot[0].Ls[0].body);
			printf("%f %f %f\n", pos1[0], pos1[1], pos1[2]);
		}
		if (!pos2) pos2 = dBodyGetPosition(robot[0].Ls[0].body);
		//printf("pos2 = [%f, %f, %f]\n", pos2[0], pos2[1], pos2[2]);
		return pos2[sur] - pos;
	}

	void dSimLoop(double i){
		//writeUparam();
		for(int k=0; k<numOfPrimitivs; k++){
			//printf("Vosiel som do dSimLoop a k = %d\n", k);
			dReal v = getU(u[k], i) - dJointGetHingeAngle(robot[k].joint);
			if (v > 1) v = 1;
			if (v < -1) v = -1;
			
			dJointSetHingeParam(robot[k].joint, dParamVel, v);
		}

		if(!isDeleted && isFree){
			for(int i = 0; i<pocetFModulov; i++){
				setFreeRotation(numsOfFModuls[i]);
			}		
		}
		//printf("Zvladol som vsetky dJointSetHingeParam\n");
		dSpaceCollide(space,0,&nearCallback);
		//printf("Zvladol som vsetky dSpaceCollide\n");
		dWorldStep(world,0.05);
		//printf("Zvladol som vsetky dWorldStep\n");
		dJointGroupEmpty(contactgroup);
		//printf("Zvladol som vsetky dJointGroupEmpty\n\n");	
	}

	void setFreeRotation(int i){
		dJointSetHingeParam(robot[i].joint, dParamVel, 0);
		dJointSetHingeParam(robot[i].joint, dParamFMax, 0);
	}

	dReal getU(Speed thisU, double thisT){
		return (thisU.A*sin(thisU.om*thisT + thisU.fi) + thisU.B);
	}

	static void nearCallback(void *data, dGeomID o1, dGeomID o2){
		if(dGeomIsSpace(o1) || dGeomIsSpace(o2)){
			dSpaceCollide2(o1, o2, data, &nearCallback);
		}else{
			int i;
			dBodyID b1 = dGeomGetBody(o1);
			dBodyID b2 = dGeomGetBody(o2);
			if(b1 && b2 && dAreConnectedExcluding(b1, b2, dJointTypeContact)) return;
		
			dContact contact[8];
			for(i=0; i<8; i++) {
				contact[i].surface.mode = dContactSoftCFM; //dContactBounce | dContactSoftERP | dContactSoftCFM; //
				contact[i].surface.mu = 5;
				contact[i].surface.soft_erp = 0.5;
				contact[i].surface.soft_cfm = 0.01;
			}
			if(int numc = dCollide(o1, o2, 8, &contact[0].geom, sizeof(dContact))) {
				for (i=0; i<numc; i++) {
					dJointID c = dJointCreateContact(world, contactgroup, contact+i);
					dJointAttach(c, b1, b2);
				}
			}
		}
	}

	const dReal *calldBodyGetPosition(){
		return dBodyGetPosition(robot[0].Ls[0].body);
	}

	void writeUparam(){
		printf("[A   , om   , fi   , B]\n");
		for (int k = 0; k < numOfPrimitivs; ++k){
			 printf("[%f, %f, %f, %f]\n", u[k].A, u[k].om, u[k].fi, u[k].B);
		}
		printf("\n");
	}

	void callSim(int argc, char *argv[]){
		prepDrawStuff();
		
		dsSimulationLoop (argc,argv,1280,720,&fn);
	}

	void prepDrawStuff() {
		fn.version = DS_VERSION;
		fn.start   = &start;
		fn.step    = &simLoop;
		fn.command = NULL;
		fn.stop    = NULL;
		fn.path_to_textures = texturePath;
	}

	static void simLoop(int pause){
		int i, j, k;
		
		if(pause){
			if(t_this<50){
				dSimLoop(t_this);
				t_this+=0.05;
			}else{
				//t_this = 0;
			}
		}
		int isBrok = 0;
		//dsSetTexture(DS_WOOD);
		for(k=0; k<numOfPrimitivs; k++){
			for(int l = 0; l<pocetFModulov; l++){
				if(k == numsOfFModuls[l]){
					isBrok = 1;
				}		
			}	
			if(isBrok && !isDeleted){
				dsSetColor(1,0,0);
				for(j=0; j<2; j++){
					drawGeom(robot[k].Ls[0].geom[j],0,0);
					drawGeom(robot[k].Ls[1].geom[j],0,0);
				}
				isBrok = 0;
			}else{
				for(i=0; i<2; i++){
					dsSetColor(0,i,1);
					for(j=0; j<2; j++){
						drawGeom(robot[k].Ls[i].geom[j],0,0);
					}
				}
			}	
		}
	}

	
	static void start(){
		dAllocateODEDataForThread(dAllocateMaskAll);

		static float pos[2][3] = {9.7023,-9.5245,7.8000,152.5000,-33.5000,0.0000};
		 // {-8.4338,4.7563,8.5700,0.0000,-47.5000,0.0000};
		// 
		// {3.9052,-5.2994,7.7800,149.0000,-53.5000,0.0000}; pictures
		// {9.7023,-9.5245,7.8000,152.5000,-33.5000,0.0000};  //CoSMO2
		// {0.6727,-9.3692,7.8000,101.5000,-43.0000,0.0000};
		// {9.7023,-9.5245,7.8000,152.5000,-33.5000,0.0000};  //CoSMO2
		// {18.6801,-4.9103,3.2100,179.0000,-12.5000,0.0000}; //Snake
		// {5.0957,-0.4233,2.2500,177.0000,-24.5000,0.0000}; 
		// {2.9110,-5.1198,6.2500,134.5000,-43.0000,0.0000}; //CoSMO

		dsSetViewpoint(pos[0], pos[1]);
	}

	void drawGeom (dGeomID g, const dReal *pos, const dReal *R){
		if (!g) return;
		if (!pos) pos = dGeomGetPosition (g);
		if (!R) R = dGeomGetRotation (g);

		if (dGeomGetClass(g) == dBoxClass) {
			dVector3 sides;
			dGeomBoxGetLengths (g,sides);
			dsDrawBox (pos,R,sides);
		}else{
			dGeomID g2 = dGeomTransformGetGeom (g);
			const dReal *pos2 = dGeomGetPosition (g2);
			const dReal *R2 = dGeomGetRotation (g2);
			dVector3 actual_pos;
			dMatrix3 actual_R;
			dMultiply0_331 (actual_pos,R,pos2);
			actual_pos[0] += pos[0];
			actual_pos[1] += pos[1];
			actual_pos[2] += pos[2];
			dMultiply0_333 (actual_R,R,R2);
			drawGeom (g2,actual_pos,actual_R);
		}
	}

	void freeAll(){
		free(robot);
		free(u);
		free(primJoint);
		for(int i=0; i<numOfPrimitivs; i++){
			free(robot_conf.boxOffset[i]);
		}
		free(robot_conf.boxOffset);
		free(robot_conf.boxOffsetAngel);
		free(robot_conf.jointWith);
	}

	void closeMyWorld(){
		for (int i = 0; i < numOfPrimitivs; i++){
			dSpaceDestroy(robot[i].space);
		}
		dJointGroupDestroy (robotJointGroup);
		dJointGroupDestroy (contactgroup);
		dSpaceDestroy (space);
		dWorldDestroy (world);
		freeAll();
		dCloseODE();
	}

	void setIsDeleted(int this_isDeleted){
		isDeleted = this_isDeleted;
	}
#ifdef __cplusplus
    }
#endif