#include "../include/MPNN/include/DNN/multiann.h"
#include "../include/MyMultiann.h"

#ifdef __cplusplus
    extern "C" {
#endif
        int dim, best_idx;
        double best_dist = INFINITY;

        MultiANN* CreateMultiANN(){
                return new MultiANN();
        }

        MultiANN* MyMultiANN(int thisDim){
            dim = thisDim;
            int topology[dim], i;
            ANNcoord scale[dim];
            for(i=0;i<dim;i++) {
                scale[i] = 1.0;
                topology[i] = 1;
            }
            return new MultiANN(dim, 1, topology,(ANNpoint) scale);
        }       

        void MyAddPoint(MultiANN* Mann, SPACE q_init, int x_ptr){
            int j;
            ANNpoint annpt = MyAnnAllocPt(dim,0);
            for(j=0;j<dim;j++) {
                annpt[j] = q_init.coordinates[j];
            }
            Mann->AddPoint(annpt, x_ptr);
        }

        int MyNearestNeighbor(MultiANN* Mann, const SPACE q_rand){
            int k;
            ANNpoint x = MyAnnAllocPt(dim,0);
            for(k=0;k<dim;k++) {
                x[k] = q_rand.coordinates[k];
            }
            return Mann->NearestNeighbor(x,best_idx,best_dist);
        }

        void  MydeletMultiANN(MultiANN* Mann){
                delete Mann;
        }
#ifdef __cplusplus
    }
#endif