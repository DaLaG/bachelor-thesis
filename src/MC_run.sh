 #!/bin/bash
 #PBS -N BT_script
 #PBS -l walltime=10m
 #PBS -l nodes=1:ppn=1:x86:nfs4
 #PBS -l mem=50mb
 #PBS -l scratch=50mb
 # direktivy si upravte/umazte dle potreb sveho vypoctu
 
 # nastaveni uklidu SCRATCHE pri chybe nebo ukonceni
 # (pokud nerekneme jinak, uklidime po sobe)
 trap 'clean_scratch' TERM EXIT
 
 DATADIR="/storage/brno2/home/lampedan/BT" # sdilene pres NFSv4
 
 cp $DATADIR/input/finish.txt $SCRATCHDIR  || exit 1
 cp $DATADIR/input/poligons.txt $SCRATCHDIR  || exit 2
 cp -rf $DATADIR/input/S-robot $SCRATCHDIR/movement  || exit 3
 cp $DATADIR/src/BT $SCRATCHDIR  || exit 4
 cd $SCRATCHDIR || exit 5
 mkdir movement/conv

 ./BT 1 4

 cp -rf movement $DATADIR/output/S-robotNew || export CLEAN_SCRATCH=true 
 #cp G.txt $DATADIR/output/S-robotNew/ || export CLEAN_SCRATCH=true
 #cp log2.txt $DATADIR/output/ 
 # cp way.txt $DATADIR/output/way_fix2_10.txt || export CLEAN_SCRATCH=true 