#! bin/bash

case "$2" in 
0) 	cp -rf ../input/Snake4 ../input/movement
	;;
1) 	cp -rf ../input/Snake5 ../input/movement
	;;
2)	cp -rf ../input/Snake6 ../input/movement
	;;
3)	cp -rf ../input/cross ../input/movement
	;;
4)	cp -rf ../input/S-robot ../input/movement
	;;
5)	cp -rf ../input/Dog ../input/movement
	;;
*) echo "out of scope"
	;;
esac

mkdir ../input/movement/conv

# valgrind --leak-check=yes ./BT $1 $2 > out.log 2> error.log 
./BT $1 $2 > out.log 2> error.log 

#case "$2" in 
#0)	cp ../input/movement -rf ../output/Snake4New
#	;;
#1)	cp ../input/movement -rf ../output/Snake5New
#	;;
#2)	cp ../input/movement -rf ../output/Snake6New
#	;;
#3)	cp ../input/movement -rf ../output/crossNew
#	;;
#4)	cp ../input/movement -rf ../output/S-robotNew
#	;;
#5)	cp ../input/movement -rf ../output/DogNew
#	;;
#*) echo "out of scope"
#	;;
#esac

rm -rf ../input/movement