#ifndef PSO_H
#define PSO_H

#include "CoSMO.h"
#include <iostream>
#include <time.h>

#ifndef OMEGA
#define OMEGA 	0.3466 //0.5
#endif

#ifndef FI_P
#define FI_P 	1.1931 //2
#endif

#ifndef FI_Q
#define FI_Q 	1.1931 //2
#endif

//#ifdef __cplusplus
//    extern "C" {
//#endif

	static int dimPSO; /* search-space dimension*/
	static int swarm_P; /* number of particles in swarm */

	int mainPSO(int argc, char *argv[],int call_PSO, char *robotType[4], int config, int isDeleted);

	int PSO_fun(char *convergPath, char *convergPath2, int I, char *robotType, int config);

	dReal readUparamPSO(char *robotType, int config);

	void writeUparamToFile(char *robotType, Speed *q);

	void writeConvergence(char *robotType1,char *robotType2, dReal *convergR, int I, dReal **conP_i, int swarm_P);

	void printResault(Speed *q);
//#ifdef __cplusplus
//    }
//#endif
#endif