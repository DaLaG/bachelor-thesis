#ifndef RRT_H
#define RRT_H

//#include "MyMultiann.h"
//#include "MyANN.h"

#include "RRT_Move.h"

/*#define DIM 2
#define boundX 100
#define boundY 100
*/
#ifdef __cplusplus
    extern "C" {
#endif

VERTEX **p_G;

int RRTmain(int K, int isDeleted);

int RRT(
	SPACE q_init, 
	POLIGON *end, 
	int K
	);

void writeWay(
	VERTEX *p_G2
	);

void writeG(
	VERTEX *p_G2[],
	int IMax
	);
#ifdef __cplusplus
	}
#endif
#endif