#ifndef RRT_MOVE_H
#define RRT_MOVE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

//#include "CoSMO.h"
#include "MyMultiann.h"
//#include "MyANN.h"

#ifdef __cplusplus
    extern "C" {
#endif
	#define boundX 50
	#define boundY 50



	typedef double angel;

	typedef enum MOVE { 
		NO,
		FINISH, 
		YES, 
	}Posun;

	typedef struct Vertex{
		conf_CoSMO *robot;
		short int typeOfMove; //0-right; 1-left; 2-forward; 3-backward; 10-begin

		struct Vertex *nearestNeighbor; 
	} VERTEX;

	typedef struct Poligon{
		int nvert;
		double *vertX;
		double *vertY;
	}POLIGON;

	//POLIGON *pMain;
	//POLIGON *q_end;

	int numOfModuls;
	int numOfPol;

	time_t t;

	Speed **q_speed;

	int numOfIter;

	VERTEX *getVerMem(conf_CoSMO* robot, int typeOfMove, VERTEX* nn);

	POLIGON *getPoligons(char const *argv, int finish);

	void setConf_CoSMO(conf_CoSMO *robot);

	void readUparam();

	SPACE getVector(double fi, double l1, double l2);

	void MyPrint(POLIGON *p, int max);

	Posun canMove(
		conf_CoSMO *x
		);

	Posun isFinish(
		dReal *q1, 
		SPACE q2
		);

	double sizeEdge(
		dReal *q1, 
		SPACE q2
		);

	int pnpoly(
		int nvert,  	// pocet vrcholov
		double *vertx, 	// pole Xovych suradnic vrcholov
		double *verty, 	// pole Yovych suradnic vrcholov
		double testx, 	// Xova suradnica testovacieho bodu
		double testy		// Yova suradnica testovacieho bodu
		);

	SPACE SpaceSum(
		SPACE a, 
		SPACE b
		);

	VERTEX* expandWay_cross(
		VERTEX *ver,
		SPACE s_end
		);

	double moveRobot(
		int typeOfMove, 
		SPACE s_end
		);

	void writeRobotCongif(conf_CoSMO *robot);

	void writeRobotCongif2(conf_CoSMO *r);
#ifdef __cplusplus
    }
#endif
#endif
