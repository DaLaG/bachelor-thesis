#ifndef CoSMO_H
#define CoSMO_H

#include <ode/ode.h>
#include <drawstuff/drawstuff.h>


//#include <string>
#ifdef __cplusplus
    extern "C" {
#endif
	//using namespace std;
	#ifdef dDOUBLE
		#define dsDrawBox dsDrawBoxD
		#define dsDrawSphere dsDrawSphereD
		#define dsDrawCylinder dsDrawCylinderD
		#define dsDrawCapsule dsDrawCapsuleD
		#define dsDrawConvex dsDrawConvexD
	#endif
	#define texturePath "/home/daniel/Dokumenty/ode-0.13/drawstuff/textures"

	#define DIM 		3
	#define LENGTH_1 	1.0  		// [dm]
	#define LENGTH_2 	0.2			// [dm]
	#define WIDTH_1 	0.2 		// [dm]
	#define WIDTH_2 	0.7			// [dm]
	#define HEIGHT_1	1.0 		// [dm]
	#define HEIGHT_2	0.8 		// [dm]
	#define DELAY1		0.145
	#define DELAY2		0.309
	#define MASS_1 		0.32 		// [kg]
	#define MASS_2 		0.18 		// [kg]
	#define KONSTANT 	0.98
	#define z0			0.6
	#define GRAVITY 	9.81

	typedef struct {
		dBodyID body;
		dGeomID geom[2];
	}L_object;

	typedef struct {
		L_object Ls[2];
		dJointID joint;
		dSpaceID space;
	}CoSMO_r;

	typedef struct {
		dReal **boxOffset;
		dReal *boxOffsetAngel;
		int *jointWith;
	}Offset;

	typedef struct {
		dReal A, om, fi, B;
	}Speed;

	typedef struct {
		double coordinates[3];
	} SPACE;

	typedef struct config_robot{
		SPACE pos[2];
		dMatrix3 R[2];
		dQuaternion quat[2];
		dReal lin_vel[2][3];
		dReal ang_vel[2][3];
	}conf_CoSMO;

	void readDeleteModules();
	
	void createMyWorld(int this_isDeleted);

	int getNumOfPrim();

	void createMyRobot(char *robotType);

	void readStartPosition(Offset* thisOs, char *robotType);

	void jointPrimitivsRobot();

	void callHingeJoint(dJointID* j, dBodyID* b1, dBodyID* b2, dReal angel, dReal X[3]);

	void callFixJoint(dJointID* j, dBodyID* b1, dBodyID* b2);

	void setOffsetPosRot2(int k);

	void setZeroHingeParam(int i);

	conf_CoSMO getPosition(int i);

	void getC_OffsetConfg();

	void setPosition(conf_CoSMO conf_C, int i);

	void setC_OffsetConfg();

	void setUparam(Speed *thisU);

	dReal returnDistance(int sur, const dReal *pos1,const dReal *pos2);

	void dSimLoop(double i);

	void setFreeRotation(int i);

	dReal getU(Speed thisU, double thisT);

	static void nearCallback(void *data, dGeomID o1, dGeomID o2);

	const dReal *calldBodyGetPosition();

	void writeUparam();

	void callSim(int argc, char *argv[]);

	void prepDrawStuff();

	static void simLoop(int pause);

	static void start();

	void drawGeom (dGeomID g, const dReal *pos, const dReal *R);

	void freeAll();

	void closeMyWorld();

	void setIsDeleted(int this_isDeleted);
#ifdef __cplusplus
    }
#endif
#endif