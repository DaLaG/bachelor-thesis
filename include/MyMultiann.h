#ifndef __MYMULTIANN_H
	#define __MYMULTIANN_H

	//#include "RRT_Move.h"
	#include "MyANN.h"

#ifdef __cplusplus
    extern "C" {
#endif
		typedef struct MultiANN MultiANN;

/*		MultiANN* MultiANN();				// constructor

		MultiANN*  MultiANN(MultiANN* ANN,					// constructor
			   int dim, 				// dimension of the space
			   ANNpoint x_coor, 		       	// coordinate of the initial point in the data structure
			   void *x_ptr, 		       	// pointer to the initial point in the data structure
			   int k, 				// number of nearest neighbors to be returned
			   int *topology, 			// topology of the space
			   ANNpoint scaling);			// scaling of the coordinates
*/
		MultiANN* CreateMultiANN();

		MultiANN*  MyMultiANN(					// constructor
			   int dim  				// dimension of the space
			   //int k, 				// number of nearest neighbors to be returned 
			   //int *topology, 			// topology of the space 
			   //ANNpoint scaling
			   );			// scaling of the coordinates

		void MyAddPoint(
				MultiANN* Mann,				// dynamic update of the data points
				SPACE q_init,		// the point's coordinates 
				int x_ptr
				);			// the pointer to the point 

		int MyNearestNeighbor(
					MultiANN* Mann,  			// 1-nearest neighbor query
			       	SPACE q_rand 	// query point  
			       	//int best_idx, 		// distance from the nearest neighbor to x (returned)
			       	//double best_dist
			       	); 	// distance from the nearest neighbor to x (returned)
		
		void  MydeletMultiANN(MultiANN* Mann);					// destructor
#ifdef __cplusplus
    }
#endif
#endif