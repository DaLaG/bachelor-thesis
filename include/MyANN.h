#ifndef MYANN_H
#define MYANN_H

#include <stdlib.h>         // standard libs
#include <stdio.h>          // standard I/O (for NULL)
//#include <iostream.h>           // I/O streams
#include <math.h>           // math includes
#include <values.h>         // special values
#include "CoSMO.h"

#ifdef __cplusplus
    extern "C" {
#endif
    
        typedef double  ANNcoord;       // coordinate data type
        typedef double  ANNdist;        // distance data type
        typedef int ANNidx;         // point index

        typedef ANNcoord *ANNpoint;     // a point
        typedef ANNpoint *ANNpointArray;    // an array of points 

        ANNpoint MyAnnAllocPt(int dim, ANNcoord c);     // coordinate value (all equal)

        ANNpointArray MyAnnAllocPts(int n, int dim);       // dimension

        void MyAnnDeallocPt(ANNpoint p);        // deallocate 1 point
           
        void MyAnnDeallocPts(ANNpointArray pa);

#ifdef __cplusplus
    }
#endif
#endif