#ifndef MAIN_H
#define MAIN_H

#include "RRT.h"
#include "PSO.h"

int main(int argc, char *argv[]);

double callRRT(int K, int isDeleted);

int isMovementGood(int K, double per, int typeOfRobot, int isDeleted);

void callPSO(int i, int k, int isDeleted);

double getPercentil(int IMax);

void readp_G();

void deleteModuls(int *numsOfModuls, int maxN);

#endif